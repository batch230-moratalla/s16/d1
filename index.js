console.log("Hello World!");

// Arithmetic Operators
let x = 3;
let y = 10;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x; // modulo
console.log("Result of the modulo operator: " + remainder);

// Assignment Operator ( = )

// Basic Assignment Operator (=)
// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2; // 10 // Reassigning through using addition operator
console.log("Result of addition operator: " + assignmentNumber);

// Addition Assignment Operator
// Short-hand version of reassigning a value with addition operator
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parenthesis

/*
     - When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
	 - The operations were done in the following order:
       1. 3 * 4 = 12
       2. 12 / 5 = 2.4
       3. 1 + 2 = 3
       4. 3 - 2.4 = 0.6
 */

// MDAS = Multiplication, Division, Addition and Subtraction
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of MDAS operation: " + mdas);

// The order of operations can be changed by adding parenthesis
// PEMDAS (Parenthesis, Exponent, Multiplication and Division, Addition and Subtraction)

/*
   	- By adding parentheses "()" to create more complex computations will change the order of operations still following the same rule.
    - The operations were done in the following order:
    1. 2 - 3 = -1
    2. 4 / 5 = 0.8
    3. 1 + -1 = 0
    4. 0 * 0.8 = 0
 */

let pemdas = 1 + (2 - 3) * (4 / 5); 
console.log("Result of PEMDAS operation: " + pemdas);

// Increment and Decrement

let z = 1;
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment (z): " + z)

// post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment (z): " + z);

// increment = z++;
// console.log(increment);

// pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement (z): " + z);

// post-decrement
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement (z): " + z);

// Type coercion
/*
 - Type coercion is the automatic or implicit conversion of values from one data type to another
 - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
- Values are automatically converted from one data type to another in order to resolve operations
*/

// String and Number
let numA = "10"; // - string becuase of ("")
let numB = 12; // - number

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);// type = string

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion); // type = number

// Boolean and Number
let numE = true + 1;
console.log(numE);
console.log(typeof numE);

let numF = false + 1;
console.log(numF);
console.log(typeof numF);

// Equality Operator (==)
// We could read our equality operator with 'is equal'
/* 
  - Checks whether the operands are equal/have the same content
  - Attempts to CONVERT AND COMPARE operands of different data types
  - Returns a boolean value
*/

let juan = 'juan';
let pedro = 'juan';

console.log("------------------");

console.log(1==1); // true
console.log(1==2); // false
console.log(1=='1'); // true
console.log(1=='one'); // false
console.log('juan'=='juan'); // true
console.log(juan == 'juan'); // true
console.log(pedro == 'juan'); // true

console.log("------------------");
// Inequality operator (!=)
// NOT equal
console.log(1 != 1); // false
console.log(1 != 2); // true
console.log(1 !='1'); // false
console.log(1 !='one'); // true
console.log('juan'!='juan'); // false
console.log(juan != 'juan'); // false
console.log(pedro != 'juan'); // false

console.log("------------------");
// Strict Equality Operator (===)
/* 
  - Checks whether the operands are equal/have the same content
  - Also COMPARES the data types of 2 values
  - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
  - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
  - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
  - Strict equality operators are better to use in most cases to ensure that data types provided are correct
*/
console.log(1 === 1); // true
console.log(1 === 2); // false
console.log(1 === '1'); // false
console.log(0 === false);  // false
console.log('juan' === 'juan'); // true
console.log(juan ==='juan'); // true
console.log(pedro === 'juan'); // true

console.log("------------------");
// Strict Inequality operator (!==)
/* 
  - Checks whether the operands are not equal/have the same content
  - Also COMPARES the data types of 2 values
*/
console.log(1 !== 1); // false
console.log(1 !== 2); // true
console.log(1 !== '1'); // true
console.log(0 !== false);  // true
console.log('juan' !== 'juan'); // false
console.log(juan !=='juan'); // false
console.log(pedro !== 'juan'); // false

console.log("------------------");
// Relational Operators
// Some comparison operators check whether one value is greater or less than to the other value

let a = 50;
let b = 65;

let isGreaterThan = a > b; // false
let isLessThan = a < b; // true
let isGTorEqual = a >= b; // false
let isLTorEqual = a <= b; // true
console.log(isLessThan);
console.log(isGreaterThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

console.log("------------------");
// Logical Operators (&& - AND, || - OR, ! - NOT)

let isLegalAge = true;
let isRegistered = true;
let isMarried = false;

// Logical and Operator (&&)
// all requirement must be met
// all requirement must be true
let isLegalAgeAndisRegistered = isLegalAge && isRegistered;
console.log("Result of Logical AND operator: " + isLegalAgeAndisRegistered);

let allCondition = isLegalAge && isRegistered && isMarried;
console.log("Result of Logical AND operator: " + allCondition);

// Logical OR Operator (||)
// Even just one is true or satisfied the output will be true
let someRequirements = isLegalAge || isMarried;
console.log("Result of Logical OR operator: " + someRequirements);

// Logical Not Operator (!)
let someRequirementsNotMet = !isRegistered; // false
console.log("Result of Logical Not operator: " + someRequirementsNotMet);

// !isLegalAge - false
// !isMarried - true































































